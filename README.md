<p align="center"><a href="https://gitlab.com/contractuo_public/assignment-project" target="_blank"><img src="https://www.contractuo.com/wp-content/uploads/2019/09/Logo-no-tagline-1.png" width="400"></a></p>

# Full Stack Developer Assignment

We’ve setup this assignment with the following goals:
* Getting to know each other a little better
* Getting to know your strengths
* Getting to know your preferences

By conducting an assignment which is bigger than one can handle in the given time, you are forced to make choices: what do you find most important; what are your ‘go-to’ tasks, etc. So, don’t worry not finishing any of the work; that’s not the goal. Do remember to look around you and ask question; grasp the opportunity to get to know us!
You will get approximately 4 hours to work on the assignment. During that time: shout if you have any questions (or a need for coffee/break). Work it out in any way that you feel is appropriate. When time is up, we ask you to present your work shortly to some of us and have a short discussion.
During the assignment we will ask you to share your screen, this allows us to get a better understanding of your approach.
## The assignment

### Prerequisites

Below you’ll find some directions to setup the development environment and project.
1.  Setup your development environment; IDE, PHP7+ with composer and Node.JS
2.	Checkout the current repository, install the dependencies with the following commands: composer install and npm install

### Project details

The assignment entails a Laravel project that contains logic for:
*	Blade and VueJS views for creating and editing a contract
*	Backend logic for creating and editing contract
*	Register, login, logout logic
*	web routes; home, create and edit contract
*	Contract table and model
*   Translations

### Assignment details / requirements

The project is basically an unfinished implementation for a platform, where users can create and manage contracts that does not necessarily contains best practice code standards.

In short the platform should: 
* Allow users to see and manage their contracts.
* Create and edit contracts.
* The Administrator should be able to manage users and their data.
* The platform has to contain unit-tests, which logic can and should be tested with automated tests?
* What information is stored in contracts? Ask - or better check https://admin.contractuo.com/login for more inspiration ;)



