<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/contracts/create', [App\Http\Controllers\ContractController::class, 'create']);
Route::post('/contracts/create', [App\Http\Controllers\ContractController::class, 'store']);
Route::get('/contracts/{id}', [App\Http\Controllers\ContractController::class, 'edit']);
