@extends('layouts.app')

@section('content')
    <div id="app">
        <edit-contract
            contract-title="{{$contractTitle}}"
            editor-content="{{$editorContent}}"
        ></edit-contract>
    </div>
@endsection
