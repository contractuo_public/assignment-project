<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createContract');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'editorContent' => 'required|string|',
                'contractTitle' => 'required|string|max:254',
            ]
        );
        $contract = Contract::create([
            "title" => $request->contractTitle,
            "content" => $request->editorContent
        ]);
        return array(
            'success' => true,
            'contract' => $contract
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        //
    }


    public function edit(int $contractId)
    {
        $contract = Contract::where('id',$contractId)->first();
        if($contract !== null) {
            $editorContent = $contract->content;
            $contractTitle = $contract->title;
            return view('editContract', compact(
                'editorContent',
                'contractTitle',
            ));
        }
        return abort(403, 'contract.errors.retrieve_contract');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $contract)
    {
        //
    }
}
